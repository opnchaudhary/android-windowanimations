package com.konnectcorp.apps.devbyteswindowanimations;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {
	Button btnDefaultAnimation, btnTranslateAnimation, btnScaleAnimation;

	ImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setupVaraibles();
	}

	private void setupVaraibles() {
		imageView = (ImageView) findViewById(R.id.imageView);
		btnDefaultAnimation = (Button) findViewById(R.id.btnDefaultAnimation);
		btnTranslateAnimation = (Button) findViewById(R.id.btnTranslateAnimation);
		btnScaleAnimation = (Button) findViewById(R.id.btnScaleAnimation);
		btnDefaultAnimation.setOnClickListener(this);
		btnTranslateAnimation.setOnClickListener(this);
		btnScaleAnimation.setOnClickListener(this);
		imageView.setOnClickListener(this);

	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imageView:
			break;
		case R.id.btnDefaultAnimation:
			startActivity(new Intent(MainActivity.this,SubActivity.class));
			break;
		case R.id.btnTranslateAnimation:
			Intent subActivity=new Intent(MainActivity.this,AnimatedSubActivity.class);
			Bundle translateBundle=ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.slide_in_left, R.anim.slide_out_left).toBundle();
			startActivity(subActivity,translateBundle);
			break;
		case R.id.btnScaleAnimation:
			BitmapDrawable drawable=(BitmapDrawable)imageView.getDrawable();
			Bitmap bitmap=drawable.getBitmap();
			Intent subActivity1=new Intent(MainActivity.this,AnimatedSubActivity.class);
			Bundle scaleBundle=ActivityOptions.makeThumbnailScaleUpAnimation(imageView, bitmap, 0, 0).toBundle();
			startActivity(subActivity1,scaleBundle);
			break;
		default:
			break;
		}

	}
}
