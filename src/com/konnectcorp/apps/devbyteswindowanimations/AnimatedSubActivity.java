package com.konnectcorp.apps.devbyteswindowanimations;

import android.app.Activity;
import android.os.Bundle;

public class AnimatedSubActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.animatedsubactivity_view);
	}

	@Override
	protected void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
	}

}
